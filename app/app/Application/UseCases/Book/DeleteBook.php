<?php

namespace App\Application\UseCases\Book;

use App\Domain\Book\Services\BookService;

readonly class DeleteBook
{
    public function __construct(
        private BookService $bookService,
    ) {
    }

    public function execute(int $id): void
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        $this->bookService->deleteBook($id);
    }
}
