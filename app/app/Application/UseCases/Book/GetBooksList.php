<?php

namespace App\Application\UseCases\Book;

use App\Domain\Book\Services\BookService;
use Illuminate\Support\Collection;

readonly class GetBooksList
{
    public function __construct(
        private BookService $bookService,
    ) {
    }

    public function execute(): Collection
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->bookService->getAllBooks();
    }
}
