<?php

namespace App\Application\UseCases\Book;

use App\Application\Dto\BookDto;
use App\Domain\Book\Services\BookService;

readonly class CreateBook
{
    public function __construct(
        private BookService $bookService,
    ) {
    }

    public function execute(array $data): BookDto
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->bookService->createBook($data);
    }
}
