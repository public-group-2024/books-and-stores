<?php

namespace App\Application\UseCases\Book;

use App\Application\Dto\BookDto;
use App\Domain\Book\Services\BookService;

readonly class UpdateBook
{
    public function __construct(
        private BookService $bookService,
    ) {
    }

    public function execute(int $id, array $data): BookDto
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->bookService->updateBook($id, $data);
    }
}
