<?php

namespace App\Application\UseCases\Store;

use App\Domain\Store\Services\StoreService;
use Illuminate\Support\Collection;

readonly class GetStoresList
{
    public function __construct(
        private StoreService $storeService,
    ) {
    }

    public function execute(): Collection
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->storeService->getAllStores();
    }
}
