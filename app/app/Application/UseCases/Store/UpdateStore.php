<?php

namespace App\Application\UseCases\Store;

use App\Application\Dto\StoreDto;
use App\Domain\Store\Services\StoreService;

readonly class UpdateStore
{
    public function __construct(
        private StoreService $storeService,
    ) {
    }

    public function execute(int $id, array $data): StoreDto
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->storeService->updateStore($id, $data);
    }
}
