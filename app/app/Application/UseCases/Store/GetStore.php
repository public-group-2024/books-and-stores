<?php

namespace App\Application\UseCases\Store;

use App\Application\Dto\StoreDto;
use App\Domain\Store\Services\StoreService;

readonly class GetStore
{
    public function __construct(
        private StoreService $storeService,
    ) {
    }

    public function execute(int $id): StoreDto
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->storeService->getStoreById($id);
    }
}
