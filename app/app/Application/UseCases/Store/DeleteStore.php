<?php

namespace App\Application\UseCases\Store;

use App\Domain\Store\Services\StoreService;

readonly class DeleteStore
{
    public function __construct(
        private StoreService $storeService,
    ) {
    }

    public function execute(int $id): void
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        $this->storeService->deleteStore($id);
    }
}
