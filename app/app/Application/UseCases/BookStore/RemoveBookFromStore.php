<?php

namespace App\Application\UseCases\BookStore;

use App\Application\Dto\BookWithStoresDto;
use App\Domain\Book\Services\BookService;

readonly class RemoveBookFromStore
{
    public function __construct(
        private BookService $bookService,
    ) {
    }

    public function execute(int $bookId, int $storeId): BookWithStoresDto
    {
        // Here we can add additional validations if necessary.
        // Main validation must be performed on the request.

        return $this->bookService->detachBookFromStore($bookId, $storeId);
    }
}
