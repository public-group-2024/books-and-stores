<?php

namespace App\Application\Dto;

use App\Domain\User\Entities\User;
use Illuminate\Support\Carbon;

class UserDto
{
    public function __construct(
        public int $id,
        public string $name,
        public string $email,
        public Carbon $created_at,
        public Carbon $updated_at,
    ) {
    }

    public static function fromUserModeltoDto(User $user): UserDto
    {
        return new self(
            id: $user->id,
            name: $user->name,
            email: $user->email,
            created_at: $user->created_at,
            updated_at: $user->updated_at
        );
    }
}
