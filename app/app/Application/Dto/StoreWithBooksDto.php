<?php

namespace App\Application\Dto;

use App\Domain\Book\Entities\Book;
use App\Domain\Store\Entities\Store;
use Illuminate\Support\Collection;

class StoreWithBooksDto
{
    public function __construct(
        public StoreDto $storeDto,
        /** @var Collection<BookDto> */
        public Collection $books,
    ) {
    }

    public static function fromStoreModeltoDto(Store $store): StoreWithBooksDto
    {
        return new self(
            storeDto: StoreDto::fromStoreModeltoDto($store),
            books: $store->books->map(fn (Book $book) => BookDto::fromBookModeltoDto($book))
        );
    }
}
