<?php

namespace App\Application\Dto;

use App\Domain\Store\Entities\Store;
use Illuminate\Support\Carbon;

class StoreDto
{
    public function __construct(
        public string $id,
        public string $name,
        public string $address,
        public bool $active,
        public Carbon $created_at,
        public Carbon $updated_at,
    ) {
    }

    public static function fromStoreModeltoDto(Store $store): StoreDto
    {
        return new self(
            id: $store->id,
            name: $store->name,
            address: $store->address,
            active: $store->active,
            created_at: $store->created_at,
            updated_at: $store->updated_at
        );
    }
}
