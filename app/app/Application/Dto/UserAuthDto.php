<?php

namespace App\Application\Dto;

use App\Domain\User\Entities\User;

class UserAuthDto
{
    public function __construct(
        public string $email,
        public string $password,
        public string $token,
    ) {
    }

    public static function fromUserModeltoDto(User $user, string $token): UserAuthDto
    {
        return new self(
            email: $user->email,
            password: $user->getAuthPassword(),
            token: $token,
        );
    }
}
