<?php

namespace App\Application\Dto;

use App\Domain\Book\Entities\Book;
use Illuminate\Support\Carbon;

class BookDto
{
    public function __construct(
        public int $id,
        public string $name,
        public string $isbn,
        public float $value,
        public Carbon $created_at,
        public Carbon $updated_at,
    ) {
    }

    public static function fromBookModeltoDto(Book $book): BookDto
    {
        return new self(
            id: $book->id,
            name: $book->name,
            isbn: $book->isbn,
            value: $book->value,
            created_at: $book->created_at,
            updated_at: $book->updated_at
        );
    }
}
