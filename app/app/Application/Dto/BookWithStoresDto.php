<?php

namespace App\Application\Dto;

use App\Domain\Book\Entities\Book;
use App\Domain\Store\Entities\Store;
use Illuminate\Support\Collection;

class BookWithStoresDto
{
    public function __construct(
        public BookDto $bookDto,
        /** @var Collection<StoreDto> */
        public Collection $stores,
    ) {
    }

    public static function fromBookModeltoDto(Book $book): BookWithStoresDto
    {
        return new self(
            bookDto: BookDto::fromBookModeltoDto($book),
            stores: $book->stores->map(fn (Store $store) => StoreDto::fromStoreModeltoDto($store))
        );
    }
}
