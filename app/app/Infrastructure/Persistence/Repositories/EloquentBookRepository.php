<?php

namespace App\Infrastructure\Persistence\Repositories;

use App\Domain\Book\Entities\Book;
use App\Domain\Book\Repositories\BookRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class EloquentBookRepository implements BookRepositoryInterface
{
    public function all(): Collection
    {
        return Book::all();
    }

    public function allFromStoreId(int $storeId): Collection
    {
        return Book::whereHas('stores', function (Builder $query) use ($storeId) {
            $query->where('stores.id', $storeId);
        })->get();
    }

    public function find(int $id): ?Book
    {
        return Book::find($id);
    }

    public function findOrFail(int $id): Book
    {
        return Book::findOrFail($id);
    }

    public function create(array $data): Book
    {
        return Book::create($data);
    }

    public function update(int $id, array $data): Book
    {
        /** @var Book $book */
        $book = Book::findOrFail($id);
        $book->update($data);

        return $book;
    }

    public function delete(int $id): Book
    {
        /** @var Book $book */
        $book = Book::findOrFail($id);
        $book->delete();

        return $book;
    }

    public function attachBookToStore(int $bookId, int $storeId): Book
    {
        /** @var Book $book */
        $book = Book::findOrFail($bookId);
        $book->stores()->syncWithoutDetaching([$storeId]);

        return $book;
    }

    public function detachBookFromStore(int $bookId, int $storeId): Book
    {
        /** @var Book $book */
        $book = Book::findOrFail($bookId);
        $book->stores()->detach($storeId);

        return $book;
    }
}
