<?php

namespace App\Infrastructure\Persistence\Repositories;

use App\Domain\Store\Entities\Store;
use App\Domain\Store\Repositories\StoreRepositoryInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;

class EloquentStoreRepository implements StoreRepositoryInterface
{
    public function all(): Collection
    {
        return Store::all();
    }

    public function allActive(): Collection
    {
        return Store::where('active', true)->get();
    }

    public function allFromBookId(int $bookId): Collection
    {
        return Store::whereHas('books', function (Builder $query) use ($bookId) {
            $query->where('books.id', $bookId);
        })->get();
    }

    public function find(int $id): ?Store
    {
        return Store::find($id);
    }

    public function findOrFail(int $id): Store
    {
        return Store::findOrFail($id);
    }

    public function create(array $data): Store
    {
        return Store::create($data);
    }

    public function update(int $id, array $data): Store
    {
        $store = Store::findOrFail($id);
        $store->update($data);

        return $store;
    }

    public function delete(int $id): Store
    {
        $store = Store::findOrFail($id);
        $store->delete();

        return $store;
    }
}
