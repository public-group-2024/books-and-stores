<?php

namespace App\Infrastructure\Persistence\Repositories;

use App\Domain\User\Entities\User;
use App\Domain\User\Repositories\UserRepositoryInterface;

class EloquentUserRepository implements UserRepositoryInterface
{
    public function getFirstByEmail(string $email): ?User
    {
        return User::where('email', '=', $email)->first();
    }
}
