<?php

namespace App\Interfaces\Http\Controllers;

use App\Application\UseCases\Store\CreateStore;
use App\Application\UseCases\Store\DeleteStore;
use App\Application\UseCases\Store\GetStore;
use App\Application\UseCases\Store\GetStoresList;
use App\Application\UseCases\Store\UpdateStore;
use App\Interfaces\Http\Requests\StoreCreateRequest;
use App\Interfaces\Http\Requests\StoreUpdateRequest;
use App\Interfaces\Http\Resources\StoreResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

class StoreController extends Controller
{
    public function __construct(
        private readonly CreateStore $createStore,
        private readonly UpdateStore $updateStore,
        private readonly GetStore $getStore,
        private readonly DeleteStore $deleteStore,
        private readonly GetStoresList $getStoresList,
    ) {
    }

    public function index(): Collection
    {
        $stores = $this->getStoresList->execute();
        return StoreResource::collection($stores);
    }

    public function store(StoreCreateRequest $request): StoreResource
    {
        $storeDto = $this->createStore->execute($request->validated());
        return new StoreResource($storeDto);
    }

    public function show($id): StoreResource
    {
        $storeDto = $this->getStore->execute($id);
        return new StoreResource($storeDto);
    }

    public function update(StoreUpdateRequest $request, $id): StoreResource
    {
        $storeDto = $this->updateStore->execute($id, $request->validated());
        return new StoreResource($storeDto);
    }

    public function destroy($id): JsonResponse
    {
        $this->deleteStore->execute($id);
        return response()->json(null, 204);
    }
}
