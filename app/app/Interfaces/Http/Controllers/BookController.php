<?php

namespace App\Interfaces\Http\Controllers;

use App\Application\UseCases\Book\CreateBook;
use App\Application\UseCases\Book\DeleteBook;
use App\Application\UseCases\Book\GetBook;
use App\Application\UseCases\Book\GetBooksList;
use App\Application\UseCases\Book\UpdateBook;
use App\Interfaces\Http\Requests\BookCreateRequest;
use App\Interfaces\Http\Requests\BookUpdateRequest;
use App\Interfaces\Http\Resources\BookResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;

class BookController extends Controller
{
    public function __construct(
        private readonly CreateBook $createBook,
        private readonly UpdateBook $updateBook,
        private readonly GetBook $getBook,
        private readonly DeleteBook $deleteBook,
        private readonly GetBooksList $getBooksList,
    ) {
    }

    public function index(): Collection
    {
        $books = $this->getBooksList->execute();
        return BookResource::collection($books);
    }

    public function store(BookCreateRequest $request): BookResource
    {
        $bookDto = $this->createBook->execute($request->validated());
        return new BookResource($bookDto);
    }

    public function show($id): BookResource
    {
        $bookDto = $this->getBook->execute($id);
        return new BookResource($bookDto);
    }

    public function update(BookUpdateRequest $request, $id): BookResource
    {
        $bookDto = $this->updateBook->execute($id, $request->validated());
        return new BookResource($bookDto);
    }

    public function destroy($id): JsonResponse
    {
        $this->deleteBook->execute($id);
        return response()->json(null, 204);
    }
}
