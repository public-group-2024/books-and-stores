<?php

namespace App\Interfaces\Http\Controllers;

use App\Application\Dto\BookWithStoresDto;
use App\Application\Dto\StoreWithBooksDto;
use App\Application\UseCases\Book\GetBook;
use App\Application\UseCases\BookStore\AddBookToStore;
use App\Application\UseCases\BookStore\RemoveBookFromStore;
use App\Application\UseCases\Store\GetStore;
use App\Domain\Book\Services\BookService;
use App\Domain\Store\Services\StoreService;
use App\Interfaces\Http\Requests\AddBookToStoreRequest;
use App\Interfaces\Http\Requests\RemoveBookFromStoreRequest;
use App\Interfaces\Http\Resources\BookWithStoresResource;
use App\Interfaces\Http\Resources\StoreWithBooksResource;
use Illuminate\Routing\Controller;

class BookStoreController extends Controller
{
    public function __construct(
        private readonly AddBookToStore $addBookToStore,
        private readonly RemoveBookFromStore $removeBookFromStore,
        private readonly GetStore $getStore,
        private readonly GetBook $getBook,
        private readonly BookService $bookService,
        private readonly StoreService $storeService,
    ) {
    }

    public function addBook(AddBookToStoreRequest $request, $storeId): BookWithStoresResource
    {
        $bookId = $request->input('book_id');
        $bookWithStoresDto = $this->addBookToStore->execute($bookId, $storeId);

        return new BookWithStoresResource($bookWithStoresDto);
    }

    public function removeBook(RemoveBookFromStoreRequest $request, $storeId): BookWithStoresResource
    {
        $bookId = $request->input('book_id');
        $bookWithStoresDto = $this->removeBookFromStore->execute($bookId, $storeId);

        return new BookWithStoresResource($bookWithStoresDto);
    }

    public function getBooksFromStore($storeId): StoreWithBooksResource
    {
        $storeDto = $this->getStore->execute($storeId);
        $booksDtoCollection = $this->bookService->getAllBooksFromStoreId($storeId);
        $storeWithBookDto = new StoreWithBooksDto($storeDto, $booksDtoCollection);

        return new StoreWithBooksResource($storeWithBookDto);
    }

    public function getStoresFromBook($bookId): BookWithStoresResource
    {
        $bookDto = $this->getBook->execute($bookId);
        $storesDtoCollection = $this->storeService->getAllStoresFromBookId($bookId);
        $bookWithStoresDto = new BookWithStoresDto($bookDto, $storesDtoCollection);

        return new BookWithStoresResource($bookWithStoresDto);
    }
}
