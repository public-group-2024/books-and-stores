<?php

namespace App\Interfaces\Http\Controllers;

use App\Application\Dto\UserDto;
use App\Domain\User\Services\UserService;
use App\Interfaces\Http\Requests\LoginRequest;
use App\Interfaces\Http\Resources\AuthSuccessfullyResource;
use App\Interfaces\Http\Resources\UserResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    public function __construct(
        private readonly UserService $userService,
    ) {
    }

    public function login(LoginRequest $request): AuthSuccessfullyResource|JsonResponse
    {
        $credentials = $request->only('email', 'password');
        $userDto = $this->userService->getUserAuthWithPasswordByEmail($credentials['email']);

        if (null === $userDto || !Hash::check($credentials['password'], $userDto->password)) {
            return response()->json(['message' => 'Invalid credentials'], 401);
        }

        return new AuthSuccessfullyResource([
            'access_token' => $userDto->token,
            'token_type' => 'Bearer',
        ]);
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->tokens()->delete();

        return response()->json(['message' => 'Logout successful'], 200);
    }

    public function me(Request $request): UserResource
    {
        return new UserResource(
            UserDto::fromUserModeltoDto($request->user())
        );
    }
}
