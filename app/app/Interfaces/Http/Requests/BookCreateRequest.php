<?php

namespace App\Interfaces\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BookCreateRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'isbn' => 'required|numeric|unique:books,isbn,' . $this->id,
            'value' => 'required|numeric'
        ];
    }
}
