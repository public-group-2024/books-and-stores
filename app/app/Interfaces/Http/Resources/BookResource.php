<?php

namespace App\Interfaces\Http\Resources;

use App\Application\Dto\BookDto;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

readonly class BookResource implements Arrayable
{
    public function __construct(
        private BookDto $dto,
    ) {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->dto->id,
            'name' => $this->dto->name,
            'isbn' => $this->dto->isbn,
            'value' => $this->dto->value,
            'created_at' => $this->dto->created_at,
            'updated_at' => $this->dto->updated_at,
        ];
    }

    public static function collection(Collection $books): Collection
    {
        return $books->map(fn(BookDto $book) => new self($book));
    }
}
