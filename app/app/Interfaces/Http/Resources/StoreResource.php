<?php

namespace App\Interfaces\Http\Resources;

use App\Application\Dto\StoreDto;
use Illuminate\Contracts\Support\Arrayable;
use Illuminate\Support\Collection;

readonly class StoreResource implements Arrayable
{
    public function __construct(
        private StoreDto $dto,
    ) {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->dto->id,
            'name' => $this->dto->name,
            'address' => $this->dto->address,
            'active' => $this->dto->active,
            'created_at' => $this->dto->created_at,
            'updated_at' => $this->dto->updated_at,
        ];
    }

    public static function collection(Collection $stores): Collection
    {
        return $stores->map(fn(StoreDto $store) => new self($store));
    }
}
