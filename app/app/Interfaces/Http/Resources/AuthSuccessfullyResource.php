<?php

namespace App\Interfaces\Http\Resources;

use Illuminate\Contracts\Support\Arrayable;

readonly class AuthSuccessfullyResource implements Arrayable
{
    public function __construct(
        private array $data,
    ) {
    }

    public function toArray(): array
    {
        return [
            'access_token' => $this->data["access_token"] ?? null,
            'token_type' => $this->data["token_type"] ?? null,
        ];
    }
}
