<?php

namespace App\Interfaces\Http\Resources;

use App\Application\Dto\StoreWithBooksDto;
use Illuminate\Contracts\Support\Arrayable;

readonly class StoreWithBooksResource implements Arrayable
{
    public function __construct(
        private StoreWithBooksDto $dto,
    ) {
    }

    public function toArray(): array
    {
        return [
            ...(new StoreResource($this->dto->storeDto))->toArray(),
            'books' => $this->dto->books,
        ];
    }
}
