<?php

namespace App\Interfaces\Http\Resources;

use App\Application\Dto\BookWithStoresDto;
use Illuminate\Contracts\Support\Arrayable;

readonly class BookWithStoresResource implements Arrayable
{
    public function __construct(
        private BookWithStoresDto $dto,
    ) {
    }

    public function toArray(): array
    {
        return [
            ...(new BookResource($this->dto->bookDto))->toArray(),
            'stores' => $this->dto->stores,
        ];
    }
}
