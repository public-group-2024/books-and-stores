<?php

namespace App\Interfaces\Http\Resources;

use App\Application\Dto\UserDto;
use Illuminate\Contracts\Support\Arrayable;

readonly class UserResource implements Arrayable
{
    public function __construct(
        private UserDto $dto,
    ) {
    }

    public function toArray(): array
    {
        return [
            'id' => $this->dto->id,
            'name' => $this->dto->name,
            'email' => $this->dto->email,
            'created_at' => $this->dto->created_at,
            'updated_at' => $this->dto->updated_at
        ];
    }
}
