<?php

namespace App\Domain\Store\Entities;

use App\Domain\Book\Entities\Book;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * @property string $id
 * @property string $name
 * @property string $address
 * @property bool $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection<Book> $books
 */
class Store extends Model
{
    protected $fillable = [
        'name',
        'address',
        'active',
    ];

    public function books(): BelongsToMany
    {
        return $this->belongsToMany(Book::class);
    }
}
