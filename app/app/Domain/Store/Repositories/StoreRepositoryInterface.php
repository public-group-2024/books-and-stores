<?php

namespace App\Domain\Store\Repositories;

use App\Domain\Store\Entities\Store;
use Illuminate\Database\Eloquent\Collection;

interface StoreRepositoryInterface
{
    /**
     * @return Collection<Store>
     */
    public function all(): Collection;

    /**
     * @return Collection<Store>
     */
    public function allActive(): Collection;

    /**
     * @return Collection<Store>
     */
    public function allFromBookId(int $bookId): Collection;

    public function find(int $id): ?Store;

    public function findOrFail(int $id): Store;

    public function create(array $data): Store;

    public function update(int $id, array $data): Store;

    public function delete(int $id): Store;
}
