<?php

namespace App\Domain\Store\Services;

use App\Domain\Store\Repositories\StoreRepositoryInterface;
use App\Application\Dto\StoreDto;
use App\Domain\Store\Entities\Store;
use Illuminate\Support\Collection;

readonly class StoreService
{
    public function __construct(
        private StoreRepositoryInterface $repository,
    ) {
    }

    /**
     * @return Collection<StoreDto>
     */
    public function getAllStores(): Collection
    {
        return Collection::make($this->repository->allActive())
            ->map(fn (Store $store) => StoreDto::fromStoreModeltoDto($store));
    }

    public function getStoreById(int $id): StoreDto
    {
        return StoreDto::fromStoreModeltoDto($this->repository->findOrFail($id));
    }

    /**
     * @return Collection<StoreDto>
     */
    public function getAllStoresFromBookId(int $bookId): Collection
    {
        return Collection::make($this->repository->allFromBookId($bookId))
            ->map(fn (Store $store) => StoreDto::fromStoreModeltoDto($store));
    }

    public function createStore(array $data): StoreDto
    {
        $store = $this->repository->create($data);
        return StoreDto::fromStoreModeltoDto($store);
    }

    public function updateStore(int $id, array $data): StoreDto
    {
        $store = $this->repository->update($id, $data);
        return StoreDto::fromStoreModeltoDto($store);
    }

    public function deleteStore(int $id): void
    {
        $this->repository->delete($id);
    }
}
