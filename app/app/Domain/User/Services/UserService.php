<?php

namespace App\Domain\User\Services;

use App\Application\Dto\UserAuthDto;
use App\Domain\User\Repositories\UserRepositoryInterface;

readonly class UserService
{
    public function __construct(
        private UserRepositoryInterface $repository,
    ) {
    }

    public function getUserAuthWithPasswordByEmail(string $email): ?UserAuthDto
    {
        $user = $this->repository->getFirstByEmail($email);

        if (null !== $user) {
            $token = $user->createToken('auth_token')->plainTextToken;
        }

        return null === $user ? null : UserAuthDto::fromUserModeltoDto($user, $token);
    }
}
