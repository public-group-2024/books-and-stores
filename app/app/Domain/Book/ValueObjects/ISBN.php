<?php

namespace App\Domain\Book\ValueObjects;

use InvalidArgumentException;

class ISBN
{
    private string $value;

    public function __construct(string $value)
    {
        if (!$this->isValidISBN($value)) {
            throw new InvalidArgumentException(
                sprintf("Invalid ISBN: %s. Valid examples: %s or %s", $value, "9781492054139", "1492054135"),
            );
        }

        $this->value = $value;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    private function isValidISBN(string $isbn): bool
    {
        // Add ISBN validation logic here (ISBN-10 or ISBN-13)
        // Simplified ISBN-13 validation example:
        if (strlen($isbn) !== 13 || !is_numeric($isbn)) {
            return false;
        }

        $sum = 0;
        for ($i = 0; $i < 12; $i++) {
            $sum += $isbn[$i] * ($i % 2 === 0 ? 1 : 3);
        }

        $checksum = 10 - ($sum % 10);
        if ($checksum == 10) {
            $checksum = 0;
        }

        return $isbn[12] == $checksum;
    }

    public function toString(): string
    {
        return $this->__toString();
    }

    public function __toString(): string
    {
        return $this->value;
    }
}
