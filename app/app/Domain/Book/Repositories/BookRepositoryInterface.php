<?php

namespace App\Domain\Book\Repositories;

use App\Domain\Book\Entities\Book;
use Illuminate\Database\Eloquent\Collection;

interface BookRepositoryInterface
{
    /**
     * @return Collection<Book>
     */
    public function all(): Collection;

    /**
     * @return Collection<Book>
     */
    public function allFromStoreId(int $storeId): Collection;

    public function find(int $id): ?Book;

    public function findOrFail(int $id): Book;

    public function create(array $data): Book;

    public function update(int $id, array $data): Book;

    public function delete(int $id): Book;

    public function attachBookToStore(int $bookId, int $storeId): Book;

    public function detachBookFromStore(int $bookId, int $storeId): Book;
}
