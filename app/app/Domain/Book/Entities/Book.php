<?php

namespace App\Domain\Book\Entities;

use App\Domain\Book\ValueObjects\ISBN;
use App\Domain\Store\Entities\Store;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Carbon;

/**
 * @property string $id
 * @property string $name
 * @property string $isbn
 * @property float $value
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection<Store> $stores
 */
class Book extends Model
{
    protected $fillable = [
        'name',
        'isbn',
        'value',
    ];

    public function stores(): BelongsToMany
    {
        return $this->belongsToMany(Store::class);
    }

    public function setIsbnAttribute(string $value): void
    {
        $this->attributes['isbn'] = (new ISBN($value))->toString();
    }
}
