<?php

namespace App\Domain\Book\Services;

use App\Application\Dto\BookWithStoresDto;
use App\Domain\Book\Entities\Book;
use App\Domain\Book\Repositories\BookRepositoryInterface;
use App\Application\Dto\BookDto;
use Illuminate\Support\Collection;

readonly class BookService
{
    public function __construct(
        private BookRepositoryInterface $repository,
    ) {
    }

    /**
     * @return Collection<BookDto>
     */
    public function getAllBooks(): Collection
    {
        return Collection::make($this->repository->all())
            ->map(fn (Book $book) => BookDto::fromBookModeltoDto($book));
    }

    public function getBookById(int $id): BookDto
    {
        return BookDto::fromBookModeltoDto($this->repository->findOrFail($id));
    }

    /**
     * @return Collection<BookDto>
     */
    public function getAllBooksFromStoreId(int $storeId): Collection
    {
        return Collection::make($this->repository->allFromStoreId($storeId))
            ->map(fn (Book $book) => BookDto::fromBookModeltoDto($book));
    }

    public function createBook(array $data): BookDto
    {
        $book = $this->repository->create($data);

        return BookDto::fromBookModeltoDto($book);
    }

    public function updateBook(int $id, array $data): BookDto
    {
        $book = $this->repository->update($id, $data);

        return BookDto::fromBookModeltoDto($book);
    }

    public function deleteBook(int $id): void
    {
        $this->repository->delete($id);
    }

    public function attachBookToStore(int $bookId, int $storeId): BookWithStoresDto
    {
        $book = $this->repository->attachBookToStore($bookId, $storeId);

        return BookWithStoresDto::fromBookModeltoDto($book);
    }

    public function detachBookFromStore(int $bookId, int $storeId): BookWithStoresDto
    {
        $book = $this->repository->detachBookFromStore($bookId, $storeId);

        return BookWithStoresDto::fromBookModeltoDto($book);
    }
}
