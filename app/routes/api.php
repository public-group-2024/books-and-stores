<?php

use App\Interfaces\Http\Controllers\BookController;
use App\Interfaces\Http\Controllers\AuthController;
use App\Interfaces\Http\Controllers\BookStoreController;
use App\Interfaces\Http\Controllers\StoreController;
use Illuminate\Support\Facades\Route;

Route::post('login', [AuthController::class, 'login']);

Route::middleware('auth:sanctum')->group(function () {
    Route::post('logout', [AuthController::class, 'logout']);
    Route::get('me', [AuthController::class, 'me']);

    Route::apiResource('books', BookController::class);
    Route::apiResource('stores', StoreController::class);

    Route::post('stores/{storeId}/books', [BookStoreController::class, 'addBook']);
    Route::delete('stores/{storeId}/books', [BookStoreController::class, 'removeBook']);
    Route::get('stores/{storeId}/books', [BookStoreController::class, 'getBooksFromStore']);
    Route::get('books/{bookId}/stores', [BookStoreController::class, 'getStoresFromBook']);
});
