# Laravel Dockerized Application with DDD

This project is a Laravel application configured to run with Docker, using Nginx, PHP 8, and MySQL 8. The goal is to provide a consistent and easy-to-configure development environment.

## Requirements

- Docker
- Docker Compose

## Notes

- This project did not consider or worry about access levels or roles
- Only authentication was implemented (no user registration)
- To log in, use: `email@email.com` and password `123456`
- Please import the Postman collection in the root of this repository `Search_and_Stay_Sample.postman_collection.json`

## Project Structure

```
project-root/
├── app/                     # Laravel code
│   ├── (Laravel code)
├── docker/
│   ├── nginx/
│   │   ├── default.conf     # Nginx configuration
│   ├── php/
│   │   ├── Dockerfile       # Dockerfile for the PHP service
│   │   ├── local.ini        # Local PHP configurations
│   │   └── xdebug.ini       # Xdebug configurations
└── docker-compose.yml       # Docker Compose configuration
```

## Setup (quickstart)

   ```sh
   git clone https://gitlab.com/public-group-2024/books-and-stores.git
   cd books-and-stores
   cp -f -R app/.env.example app/.env
   docker-compose up -d --build
   docker-compose exec app composer install
   docker-compose exec app php artisan key:generate
   docker-compose exec app chmod -R 777 storage
   sudo chown -R $(whoami):$(whoami) .
   docker-compose exec app php artisan migrate:fresh --seed --path=app/Infrastructure/Persistence/Migrations
   ```
Open your browser and navigate to [http://localhost](http://localhost).

## Postman Collection

You can import the collection `Search_and_Stay_Sample.postman_collection.json` to your `Postman` in order to test all requests

## Debugging with Xdebug

To use Xdebug, configure your IDE (e.g., VSCode, PHPStorm) to listen for debug connections on the default Xdebug port (`9003` for Xdebug 3). Ensure your IDE is set up to accept connections.

## Useful Commands

- **Stop the containers:**

  ```sh
  docker-compose down
  ```

- **Rebuild the containers:**

  ```sh
  docker-compose up -d --build
  ```

- **Access the app container:**

  ```sh
  docker-compose exec app bash
  ```

- **Check Nginx logs:**

  ```sh
  docker-compose logs webserver
  ```

## File Structure

- **`docker/php/Dockerfile`**: Defines the PHP environment with Xdebug.
- **`docker/nginx/default.conf`**: Nginx configuration to serve the Laravel application.
- **`docker/php/local.ini`**: Custom PHP configurations.
- **`docker/php/xdebug.ini`**: Xdebug configurations.

## Final Considerations

This project is set up to provide a consistent and easy-to-use Laravel development environment. Feel free to adjust the configurations as needed to meet your specific requirements.

If you encounter any issues or have questions, please open an issue in the repository.
